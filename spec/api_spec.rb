require 'grape'
require 'sequel'
require 'pg'
require '../app/api/my_app.rb'
require 'rack/test'

describe MyApp do
  include Rack::Test::Methods

  def app
    MyApp
  end

  before do
    reset_tables()
  end

  describe 'GET /api/people' do
    it "gets all unique people" do
      get '/api/people'
      expect(last_response).to be_ok
      expect(last_response.body).to eq('{"1":"sergei","2":"sebastian","3":"albert"}')
    end
  end
  describe 'GET /api/pizza_consumptions' do
    it 'gets all ordered pizza' do
      get '/api/pizza_consumptions'
      expect(last_response).to be_ok
      expect(last_response.body).to eq('{"2015-01-01":["pepperoni","sausage"],"2015-01-02":["sausage"],"2015-01-03":["pineapple"],"2015-01-06":["pepperoni","pepperoni"],"2015-01-07":["sausage","sausage","pineapple"],"2015-01-08":["pineapple"],"2015-01-09":["pepperoni"],"2015-01-10":["sausage"],"2015-01-11":["pineapple"],"2015-01-12":["pineapple"],"2015-01-13":["sausage"],"2015-01-15":["pepperoni"],"2015-01-17":["pineapple"],"2015-03-01":["sausage","pineapple"],"2015-08-01":["pepperoni"],"2015-02-01":["pepperoni"],"2015-04-01":["pepperoni"],"2015-05-01":["sausage","pineapple","pepperoni"]}')
    end
  end

  describe 'GET /api/pizza_streaks' do
    it 'gets streaks of increasing pizza' do
      get '/api/pizza_streaks'
      expect(last_response).to be_ok
      expect(last_response.body).to eq('{"0":["2015-01-03","2015-01-06","2015-01-07"]}')
    end
    it 'gets streaks given more data' do
      DB[:csv_data].insert(:name=> "Bill", :meat=> "ham", :eaten_on=> "2015-10-1")
      DB[:csv_data].insert(:name=> "Bill", :meat=> "ham", :eaten_on=> "2015-10-2")
      DB[:csv_data].insert(:name=> "Bill", :meat=> "ham", :eaten_on=> "2015-10-2")
      update_tables()
      get '/api/pizza_streaks'
      expect(last_response).to be_ok
      expect(last_response.body).to eq('{"0":["2015-01-03","2015-01-06","2015-01-07"],"1":["2015-10-01","2015-10-02"]}');
    end
  end

  describe 'Get /api/most_pizza_day' do
    it 'gets day of the month with the most pizza' do
      get '/api/most_pizza_day'
      expect(last_response).to be_ok
      expect(last_response.body).to eq('{"2015-01":"07","2015-02":"01","2015-03":"01","2015-04":"01","2015-05":"01","2015-08":"01"}')
    end
    it 'gets day of month with most pizza given more data' do
      DB[:csv_data].insert(:name=> "Bill", :meat=> "ham", :eaten_on=> "2015-10-2")
      DB[:csv_data].insert(:name=> "Bill", :meat=> "ham", :eaten_on=> "2015-10-1")
      DB[:csv_data].insert(:name=> "Bill", :meat=> "ham", :eaten_on=> "2015-10-2")
      DB[:csv_data].insert(:name=> "Bill", :meat=> "ham", :eaten_on=> "2015-01-6")
      DB[:csv_data].insert(:name=> "Bill", :meat=> "ham", :eaten_on=> "2015-01-6")
      DB[:csv_data].insert(:name=> "Bill", :meat=> "ham", :eaten_on=> "2015-01-6")
      update_tables()
      get '/api/most_pizza_day'
      expect(last_response).to be_ok
      expect(last_response.body).to eq('{"2015-01":"06","2015-02":"01","2015-03":"01","2015-04":"01","2015-05":"01","2015-08":"01","2015-10":"02"}')
    end
  end
end

def reset_tables
  # Refreshes CSV_DATA table
  DB.run "DROP TABLE IF EXISTS csv_data"
  DB.create_table(:csv_data) do
    String :name
    String :meat
    date :eaten_on
  end

  # Copies data from data.csv and inserts into csv_data table
  cur_directory = File.dirname(__FILE__)
  DB.run "COPY csv_data FROM '" + cur_directory + "/data.csv' DELIMITER ',' CSV HEADER;"

  # Refreshes PEOPLE table
  DB.run "DROP TABLE IF EXISTS people CASCADE"
  DB.create_table(:people) do
    primary_key :id
    String :name, :unique=>true
  end
  DB.fetch('SELECT name FROM csv_data GROUP BY name HAVING COUNT(*) >= 1') do |row|
    DB[:people].insert(row)
  end

  # Refreshes MEATS table
  DB.run "DROP TABLE IF EXISTS meats CASCADE"
  DB.create_table(:meats) do
    primary_key :id
    String :meat, :unique=>true
  end
  DB.fetch('SELECT meat FROM csv_data GROUP BY meat HAVING COUNT(*) >= 1') do |row|
    DB[:meats].insert(row)
  end

  # Refreshes PIZZA_DATA table
  DB.run "DROP TABLE IF EXISTS pizza_data"
  DB.create_table(:pizza_data) do
    primary_key :id
    foreign_key :p_id, :people
    foreign_key :m_id, :meats
    date :eaten_on
  end
  DB.fetch('SELECT name, meat, eaten_on FROM csv_data') do |row|
    DB.fetch('SELECT id, name FROM people WHERE (name = ' + "'" + row[:name].to_s + "'" + ')') do |p_row|
      row[:p_id] = p_row[:id]
    end
    DB.fetch('SELECT id, meat FROM meats WHERE (meat = ' + "'" + row[:meat].to_s + "'" + ')') do |m_row|
      row[:m_id] = m_row[:id]
    end
    row.delete(:name)
    row.delete(:meat)
    DB[:pizza_data].insert(row)
  end

end

def update_tables
  # Refreshes PEOPLE table
  DB.run "DROP TABLE IF EXISTS people CASCADE"
  DB.create_table(:people) do
    primary_key :id
    String :name, :unique=>true
  end
  DB.fetch('SELECT name FROM csv_data GROUP BY name HAVING COUNT(*) >= 1') do |row|
    DB[:people].insert(row)
  end

  # Refreshes MEATS table
  DB.run "DROP TABLE IF EXISTS meats CASCADE"
  DB.create_table(:meats) do
    primary_key :id
    String :meat, :unique=>true
  end
  DB.fetch('SELECT meat FROM csv_data GROUP BY meat HAVING COUNT(*) >= 1') do |row|
    DB[:meats].insert(row)
  end

  # Refreshes PIZZA_DATA table
  DB.run "DROP TABLE IF EXISTS pizza_data"
  DB.create_table(:pizza_data) do
    primary_key :id
    foreign_key :p_id, :people
    foreign_key :m_id, :meats
    date :eaten_on
  end
  DB.fetch('SELECT name, meat, eaten_on FROM csv_data') do |row|
    DB.fetch('SELECT id, name FROM people WHERE (name = ' + "'" + row[:name].to_s + "'" + ')') do |p_row|
      row[:p_id] = p_row[:id]
    end
    DB.fetch('SELECT id, meat FROM meats WHERE (meat = ' + "'" + row[:meat].to_s + "'" + ')') do |m_row|
      row[:m_id] = m_row[:id]
    end
    row.delete(:name)
    row.delete(:meat)
    DB[:pizza_data].insert(row)
  end
end
