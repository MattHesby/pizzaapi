require 'grape'
require 'sequel'
require 'pg'

#Connects to DB
DB = Sequel.postgres("pizza",:host=>'localhost', :user=>'pizzaguy', :password=>'tasty_pizza')

# Refreshes CSV_DATA table
DB.run "DROP TABLE IF EXISTS csv_data"
DB.create_table(:csv_data) do
  String :name
  String :meat
  date :eaten_on
end

# Copies data from data.csv and inserts into csv_data table
cur_directory = File.dirname(__FILE__)
DB.run "COPY csv_data FROM '" + cur_directory + "/data.csv' DELIMITER ',' CSV HEADER;"

# Refreshes PEOPLE table
DB.run "DROP TABLE IF EXISTS people CASCADE"
DB.create_table(:people) do
  primary_key :id
  String :name, :unique=>true
end
DB.fetch('SELECT name FROM csv_data GROUP BY name HAVING COUNT(*) >= 1') do |row|
  DB[:people].insert(row)
end

# Refreshes MEATS table
DB.run "DROP TABLE IF EXISTS meats CASCADE"
DB.create_table(:meats) do
  primary_key :id
  String :meat, :unique=>true
end
DB.fetch('SELECT meat FROM csv_data GROUP BY meat HAVING COUNT(*) >= 1') do |row|
  DB[:meats].insert(row)
end

# Refreshes PIZZA_DATA table
DB.run "DROP TABLE IF EXISTS pizza_data"
DB.create_table(:pizza_data) do
  primary_key :id
  foreign_key :p_id, :people
  foreign_key :m_id, :meats
  date :eaten_on
end
DB.fetch('SELECT name, meat, eaten_on FROM csv_data') do |row|
  DB.fetch('SELECT id, name FROM people WHERE (name = ' + "'" + row[:name].to_s + "'" + ')') do |p_row|
    row[:p_id] = p_row[:id]
  end
  DB.fetch('SELECT id, meat FROM meats WHERE (meat = ' + "'" + row[:meat].to_s + "'" + ')') do |m_row|
    row[:m_id] = m_row[:id]
  end
  row.delete(:name)
  row.delete(:meat)
  DB[:pizza_data].insert(row)
end

# Defines MyApp
class MyApp < Grape::API
  format :json

  # Responds with all people
  get '/api/people' do
    results = {}
    DB.fetch('SELECT * FROM people') do |row|
      results[row[:id]] = row[:name]
    end
    results
  end

  # Responds with all Pizza Consumptions
  get '/api/pizza_consumptions' do
    results = {}
    DB.fetch('SELECT meats.meat, meats.id, pizza_data.eaten_on FROM meats, pizza_data WHERE meats.id = pizza_data.m_id ') do |row|
      if(!results[row[:eaten_on]])
         results[row[:eaten_on]] = []
      end
      results[row[:eaten_on]].push(row[:meat])
    end
    results
  end

  # Responds with all streaks of increased pizza consumption
  # ie: Lists days when more pizza is consumed each consecutive day
  get '/api/pizza_streaks' do
    data = {}
    # Retrieve data from DB and organize in hash table.  Key is date, value is an array of each pizza meat bought
    DB.fetch('SELECT meats.meat, meats.id, pizza_data.eaten_on FROM meats, pizza_data WHERE meats.id = pizza_data.m_id ') do |row|
      if(!data[row[:eaten_on]])
         data[row[:eaten_on]] = []
      end
      data[row[:eaten_on]].push(row[:meat])
    end
    # Make sure the data is in chronological order
    data = Hash[data.sort]
    # Determine streaks and adds them to streaks hash
    count = 0
    streaks = {}
    current_streak = []
    last_pizza_amount = 0
    data.each do |key, array|
      if(current_streak.length == 0) # If nothing in current streak
        current_streak.push(key)
        last_pizza_amount = array.length
      elsif(array.length > last_pizza_amount) # If streak continues
        current_streak.push(key)
        last_pizza_amount = array.length
      elsif(array.length <= last_pizza_amount && current_streak.length > 1) # if streak stops but streak is long enough to add
        streaks[count] = current_streak.clone
        current_streak.clear
        last_pizza_amount = 0
        count = count + 1
      else # not a streak, and only 1 thing in current streak
        current_streak.clear
        last_pizza_amount = 0
      end

      # In-case at end of data and last item is a streak
      if (key == data.keys.last && current_streak.length > 1)
        streaks[count] = current_streak.clone
      end
    end
    streaks
  end

  # Responds with the day of each month that the most pizza was consumed
  get '/api/most_pizza_day' do
    data = {}
    # Retrieve data from DB and organize in hash table.  Key is date, value is an array of each pizza meat bought
    DB.fetch('SELECT meats.meat, meats.id, pizza_data.eaten_on FROM meats, pizza_data WHERE meats.id = pizza_data.m_id ') do |row|
      if(!data[row[:eaten_on]])
         data[row[:eaten_on]] = []
      end
      data[row[:eaten_on]].push(row[:meat])
    end
    # Make sure the data is in chronological order
    data = Hash[data.sort]

    # Determines the actual day in each month with the most pizzas bought
    results = {}
    current_pizza_amount = data.values[0].length
    current_year = Date.strptime(data.keys[0].to_s, "%Y-%m-%d").strftime("%Y")
    current_month = Date.strptime(data.keys[0].to_s, "%Y-%m-%d").strftime("%m")
    current_best_day = Date.strptime(data.keys[0].to_s, "%Y-%m-%d").strftime("%d")
    data.each do |key, array|
      key_year = Date.strptime(key.to_s, "%Y-%m-%d").strftime("%Y")
      key_month = Date.strptime(key.to_s, "%Y-%m-%d").strftime("%m")
      key_day = Date.strptime(key.to_s, "%Y-%m-%d").strftime("%d")

      if(current_month != key_month || current_year != key_year) # If new month or year
        current_year = key_year
        current_month = key_month
        current_best_day = key_day
        current_pizza_amount = array.length
        temp_key = key_year + "-" + key_month
        results[temp_key] = key_day
      elsif (array.length > current_pizza_amount) # If the current key has more pizza
        current_best_day = key_day
        current_pizza_amount = array.length
        temp_key = key_year + "-" + key_month
        results[temp_key] = key_day
      end
    end
    results
  end
end
