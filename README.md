# Required
- ruby
- postgresql

# to setup
- bundle install
- sudo -u postgres psql
- CREATE USER pizzaguy WITH PASSWORD 'tasty_pizza';
- ALTER USER pizzaguy WITH SUPERUSER;
- CREATE DATABASE pizza OWNER pizzaguy;
- \q

# to run
- rackup
